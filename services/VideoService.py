import sys

sys.path.append('/root/NI-MVI-local/services')

import cv2
import os
import shutil
import concurrent.futures
import moviepy.editor as mp
import ImageService as imageService


def get_frame_rate(video_file):
    # Create a VideoCapture object and read from input file
    cap = cv2.VideoCapture(video_file)

    # Check if camera opened successfully
    if not cap.isOpened():
        print("Error opening video file")
        return

    # Read frame rate and calculate time interval between frames
    frame_rate = cap.get(cv2.CAP_PROP_FPS)
    time_interval = 1 / frame_rate

    # When everything done, release the video capture object
    cap.release()
    return frame_rate


def video_to_frames_concurrent(video_file, output_dir, image_format='.png'):
    video_name = os.path.basename(video_file)
    shutil.rmtree(output_dir, ignore_errors=True, onerror=None)
    os.makedirs(output_dir, exist_ok=True)
    # Create a VideoCapture object and read from input file
    cap = cv2.VideoCapture(video_file)
    count = 0

    # Check if camera opened successfully
    if cap.isOpened()== False:
        print("Error opening video file")
        return

    # Get total number of frames in video
    total_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

    # Use a ThreadPoolExecutor to generate the frames concurrently
    with concurrent.futures.ThreadPoolExecutor() as executor:
        futures = []
        # Read until video is completed
        while(cap.isOpened()):
            # Capture frame-by-frame
            ret, frame = cap.read()
            if ret:
                # Save the frame as an image file in a separate thread
                image_file = output_dir + '/'+video_name+'{:06d}'.format(count) + image_format
                future = executor.submit(cv2.imwrite, image_file, frame)
                futures.append(future)
                count += 1
                if count % 100 == 0:
                    print(f'Processed frame {count} of {total_frames}')
            else:
                break

    # Wait for all threads to complete
    concurrent.futures.wait(futures)
    # When everything done, release the video capture object
    cap.release()
    print(f'Finished processing {count} frames')


def create_video(image_folder, video_name, fps, image_formats=".png"):
    # Create the parent directories if they don't exist
    os.makedirs(os.path.dirname(video_name), exist_ok=True)
    print(f"Creating video: {video_name}")

    # Accept multiple image formats
    images = [img for img in os.listdir(image_folder) if any(img.endswith(fmt) for fmt in image_formats)]
    images = sorted(images)
    frame = cv2.imread(os.path.join(image_folder, images[0]))
    height, width, layers = frame.shape
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    # Set the fps using the custom parameter
    video = cv2.VideoWriter(video_name, fourcc, fps, (width, height))
    count = 0
    for image in images:
        if count % 1000 == 0:
            print(f"Processed {count}/{len(images)}")
        count += 1
        video.write(cv2.imread(os.path.join(image_folder, image)))

    video.release()
    print(f"Video created: {video_name}")


def add_audio_from_video_to_video(source_video_path, target_video_path):
    # Load the source video
    source_video = mp.VideoFileClip(source_video_path)
    # Extract the audio from the source video
    audio = source_video.audio
    # Load the target video
    target_video = mp.VideoFileClip(target_video_path)
    # Add the audio to the target video
    target_video = target_video.set_audio(audio)
    # Save the modified target video to the same file
    target_video.write_videofile(target_video_path)


def prepare_for_training(path_to_videos, output_path):
    index = 0
    # Iterate over all the files in the input directory
    for video_name in os.listdir(path_to_videos):
        # Construct the full path to the file
        file_path = os.path.join(path_to_videos, video_name)
        # Check if the file is a video file
        if video_name.endswith('.mp4') or video_name.endswith('.avi'):
            # Call the video_to_frames_concurrent function on the file
            video_to_frames_concurrent(file_path, output_path+'/scene_'+str(index))
            index += 1

