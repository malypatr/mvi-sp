import matplotlib.pyplot as plt
import numpy as np


def generate_superresolution(generator, src_image, tar_image):
    # generate image from source
    gen_image = generator.predict(src_image)

    plt.figure(figsize=(16, 8))
    plt.subplot(231)
    plt.title('LR Image')
    plt.imshow(src_image[0, :, :, :])
    plt.subplot(232)
    plt.title('Superresolution')
    plt.imshow(gen_image[0, :, :, :])
    plt.subplot(233)
    plt.title('Orig. HR image')
    plt.imshow(tar_image[0, :, :, :])
    plt.show()

