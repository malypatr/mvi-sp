import datetime
import os
import shutil
import subprocess


def mycall(cmd):
    process = subprocess.Popen(cmd)
    # Wait for the process to finish
    output, error = process.communicate()
    print(output)


def download_model():
    shutil.rmtree("./model", ignore_errors=True, onerror=None)
    os.makedirs("./model", exist_ok=True)
    # cmd = ['ls','-l']
    cmd = ['wget', 'https://ge.in.tum.de/download/data/TecoGAN/model.zip', '-O', './model/model.zip']
    mycall(cmd)
    cmd = ['unzip', './model/model.zip', '-d', './model']
    mycall(cmd)
    cmd = ['rm', './model/model.zip']
    mycall(cmd)


def download_teco_gen():
    shutil.rmtree("./TecoGEN")
    cmd = ["!git", "clone", "https://github.com/RAFALAMAO/TecoGAN.git"]
    mycall(cmd)

    download_model()


def call_super_resolution(output_file, lr_images_dir):
    parent_directory = os.path.dirname(output_file)
    os.makedirs(output_file, exist_ok=True)

    cmd = ["python3", "./TecoGAN/main.py",
           # "--cudaID", "0",  # set the cudaID here to use only one GPU
           "--output_dir", output_file,  # Set the place to put the results.
           "--summary_dir", os.path.join(parent_directory, 'log/'),  # Set the place to put the log.
           "--mode", "inference",
           "--input_dir_LR", lr_images_dir,  # the LR directory
           # "--input_dir_HR", os.path.join("./HR/", testpre[nn]),  # the HR directory
           # one of (input_dir_HR,input_dir_LR) should be given
           "--output_pre", output_file,  # the subfolder to save current scene, optional
           "--num_resblock", "16",  # our model has 16 residual blocks,
           # the pre-trained FRVSR and TecoGAN mini have 10 residual blocks
           "--checkpoint", './model/TecoGAN',  # the path of the trained model,
           "--output_ext", "png"  # png is more accurate, jpg is smaller
           ]
    print("Running: ")
    print(" ".join(map(str, cmd)))
    print("Start: ")
    mycall(cmd)


def prepare_vgg_19():
    '''
    In order to use the VGG as a perceptual loss,
    we download from TensorFlow-Slim image classification model library:
    https://github.com/tensorflow/models/tree/master/research/slim
    '''
    VGGPath = "./model"  # the path for the VGG model, there should be a vgg_19.ckpt inside
    VGGModelPath = os.path.join(VGGPath, "vgg_19.ckpt")
    if not os.path.exists(VGGPath):
        os.mkdir(VGGPath)
    if not os.path.exists(VGGModelPath):
        # Download the VGG 19 model from
        print("VGG model not found, downloading to %s" % VGGPath)
        cmd0 = "wget http://download.tensorflow.org/models/vgg_19_2016_08_28.tar.gz -O " + os.path.join(VGGPath,
                                                                                                        "vgg19.tar.gz")
        cmd0 += ";tar -xvf " + os.path.join(VGGPath, "vgg19.tar.gz") + " -C " + VGGPath + "; rm " + os.path.join(
            VGGPath, "vgg19.tar.gz")
        subprocess.call(cmd0, shell=True)
    return VGGModelPath


def prepare_pre_trained_frvsr():
    '''
    Use our pre-trained FRVSR model. If you want to train one, try runcase 4, and update this path by:
    FRVSRModel = "ex_FRVSRmm-dd-hh/model-500000"
    '''
    FRVSRModel = "model/ourFRVSR"
    if not os.path.exists(FRVSRModel + ".data-00000-of-00001"):
        # Download our pre-trained FRVSR model
        print("pre-trained FRVSR model not found, downloading")
        cmd0 = "wget http://ge.in.tum.de/download/2019-TecoGAN/FRVSR_Ours.zip -O model/ofrvsr.zip;"
        cmd0 += "unzip model/ofrvsr.zip -d model; rm model/ofrvsr.zip"
        subprocess.call(cmd0, shell=True)
        '''
    loading the pre-trained model from FRVSR can make the training faster
    --checkpoint, path of the model, here our pre-trained FRVSR is given
    --pre_trained_model,  to continue an old (maybe accidentally stopeed) training, 
        pre_trained_model should be false, and checkpoint should be the last model such as 
        ex_TecoGANmm-dd-hh/model-xxxxxxx
        To start a new and different training, pre_trained_model is True. 
        The difference here is 
        whether to load the whole graph icluding ADAM training averages/momentums/ and so on
        or just load existing pre-trained weights.
    '''
    cmd = [  # based on a pre-trained FRVSR model. Here we want to train a new adversarial training
        "--pre_trained_model",  # True
        "--checkpoint", FRVSRModel,
    ]
    return cmd


def prepare_training_data(TrainingDataPath):
    '''Video Training data:
    please udate the TrainingDataPath according to ReadMe.md
    input_video_pre is hard coded as scene in dataPrepare.py at line 142
    str_dir is the starting index for training data
    end_dir is the ending index for training data
    end_dir+1 is the starting index for validation data
    end_dir_val is the ending index for validation data
    max_frm should be duration (in dataPrepare.py) -1
    queue_thread: how many cpu can be used for loading data when training
    name_video_queue_capacity, video_queue_capacity: how much memory can be used
    '''
    cmd1 = [
        "--input_video_dir", TrainingDataPath,
        "--input_video_pre", "scene",
        "--str_dir", "0",
        "--end_dir", "10",
        "--end_dir_val", "15",
        "--max_frm", "1000",
        # -- cpu memory for data loading --
        "--queue_thread", "12",  # Cpu threads for the data. >4 to speedup the training
        "--name_video_queue_capacity", "1024",
        "--video_queue_capacity", "1024",
    ]
    return cmd1


def prepare_GAN_training_parameters():
    ''' parameters for GAN training '''
    cmd1 = [
        "--ratio", "0.01",  # the ratio for the adversarial loss from the Discriminator to the Generator
        "--Dt_mergeDs",  # if Dt_mergeDs == False, only use temporal inputs, so we have a temporal Discriminator
        # else, use both temporal and spatial inputs, then we have a Dst, the spatial and temporal Discriminator
    ]
    ''' if the generator is pre-trained, to fade in the discriminator is usually more stable.
    the weight of the adversarial loss will be weighed with a weight, started from Dt_ratio_0, 
    and increases until Dt_ratio_max, the increased value is Dt_ratio_add per training step
    For example, fading Dst in smoothly in the first 4k steps is 
    "--Dt_ratio_max", "1.0", "--Dt_ratio_0", "0.0", "--Dt_ratio_add", "0.00025"
    '''
    cmd1 += [  # here, the fading in is disabled
        "--Dt_ratio_max", "1.0",
        "--Dt_ratio_0", "1.0",
        "--Dt_ratio_add", "0.0",
    ]
    ''' Other Losses '''
    cmd1 += [
        "--pingpang",  # our Ping-Pang loss
        "--pp_scaling", "0.5",  # the weight of the our bi-directional loss, 0.0~0.5
        "--D_LAYERLOSS",  # use feature layer losses from the discriminator
    ]
    return cmd1


def train_model(traing_video_folder_path):
    VGGModelPath = prepare_vgg_19()

    '''Prepare Training Folder'''
    # path appendix, manually define it, or use the current datetime, now_str = "mm-dd-hh"
    now_str = datetime.datetime.now().strftime("%m-%d-%H")
    train_dir = "trained_models/"+now_str
    shutil.rmtree(train_dir, ignore_errors=True)
    os.makedirs(train_dir, exist_ok=True)
    # train TecoGAN, loss = l2 + VGG54 loss + A spatio-temporal Discriminator
    cmd1 = ["python3", "./TecoGAN/main.py",
            "--cudaID", "0",  # set the cudaID here to use only one GPU
            "--output_dir", train_dir,  # Set the place to save the models.
            "--summary_dir", os.path.join(train_dir, "log/"),  # Set the place to save the log.
            "--mode", "train",
            "--batch_size", "4",  # small, because GPU memory is not big
            "--RNN_N", "10",  # train with a sequence of RNN_N frames, >6 is better, >10 is not necessary
            "--movingFirstFrame",  # a data augmentation
            "--random_crop",
            "--crop_size", "32",
            "--learning_rate", "0.00005",
            # -- learning_rate step decay, here it is not used --
            "--decay_step", "500000",
            "--decay_rate", "1.0",  # 1.0 means no decay
            "--stair",
            "--beta", "0.9",  # ADAM training parameter beta
            "--max_iter", "500000",  # 500k or more, the one we present is trained for 900k
            "--save_freq", "10000",  # the frequency we save models
            # -- network architecture parameters --
            "--num_resblock", "16",  # FRVSR and TecoGANmini has num_resblock as 10. The TecoGAN has 16.
            # -- VGG loss, disable with vgg_scaling < 0
            "--vgg_scaling", "0.2",
            "--vgg_ckpt", VGGModelPath,  # necessary if vgg_scaling > 0
            ]
    cmd1 += prepare_training_data(traing_video_folder_path)
    cmd1 += prepare_pre_trained_frvsr()
    cmd1 += prepare_GAN_training_parameters()
    mycall(cmd1)
