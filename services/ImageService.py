from PIL import Image
from tqdm import tqdm
import os
import PIL
import glob
import requests
import zipfile
import concurrent.futures
import time

import Properties as prop


def download_file(url, filepath):
    # Download the file
    response = requests.get(url, stream=True)
    # Use tqdm to display the download progress
    with open(filepath, "wb") as f:
        total_size = int(response.headers.get("Content-Length", 0))
        pbar = tqdm(total=total_size, unit="B", unit_scale=True)
        for data in response.iter_content(chunk_size=1024):
            f.write(data)
            pbar.update(len(data))
        pbar.close()
    return response


def download_and_extract_zip(url, directory):
    os.makedirs(directory, exist_ok=True)
    print(f"Downloading zip file from {url}")
    # Download the zip file
    temp_file_path = "temp.zip"
    if not os.path.exists(temp_file_path):
        response = download_file(url, temp_file_path)

    print(f"Extracting zip file to {directory}")
    # Extract the zip file to the specified directory
    count = 0
    with zipfile.ZipFile("temp.zip", "r") as zip_ref:
        for name in zip_ref.infolist():
            if count % 1000 == 0:
                print(f"Extracting {name} to {os.path.join(directory, name.filename)}")
            count += 1
            if name.filename.endswith('.jpg'):
                zip_ref.extract(name, directory)
                filename = os.path.basename(name.filename)
                os.rename(os.path.join(directory, name.filename), os.path.join(directory, filename))
    print("Finished extracting zip file")


def resize_and_save(filepath, target_size, output_filepath):
    with Image.open(filepath) as image:
        image = image.resize(target_size)
    # Save the resized image to the output folder
    image.save(output_filepath)


def resize_images_in_folder_concurrent(folder, target_size, output_folder):
    os.makedirs(folder, exist_ok=True)
    os.makedirs(output_folder, exist_ok=True)
    # Iterate over the files in the folder
    items = os.listdir(folder)
    num_items = len(items)
    with concurrent.futures.ThreadPoolExecutor() as executor:
        futures = []
        for file in items:
            # Check if the file is an image file
            _, extension = os.path.splitext(file)
            if extension.lower() in [".jpg", ".png", ".gif"]:
                filepath = os.path.join(folder, file)
                output_filepath = os.path.join(output_folder, file)
                future = executor.submit(resize_and_save, filepath, target_size, output_filepath)
                futures.append(future)
        _, not_finished = concurrent.futures.wait(futures, timeout=5)
        print(f"Frame processing remaining: {len(not_finished)}/{str(num_items)}")
        while len(not_finished) > 0:
            print(f"Frame processing remaining: {len(not_finished)}/{str(num_items)}")
            _, not_finished = concurrent.futures.wait(futures, timeout=5)
    print("Finished resizing images")

