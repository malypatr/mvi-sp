# Practical use of TecoGAN for video upscaling of anime

## Assigment

Use GAN to upscale anime videos.

## Instructions

1. Get an NVIDIA GPU
1. Install Jupiter and python
1. Get your own anime video or use the one provided in the repository
1. Edit the part with variables to suit your specific needs
1. Run the whole notebook


## Results

### High resolution
![HR Video](/data/videos/hr_video.avi)
### Low resolution
![LR Video](/data/videos/lr_video.avi)
### Super resolution
![SR Video](/data/videos/sr_video.avi)